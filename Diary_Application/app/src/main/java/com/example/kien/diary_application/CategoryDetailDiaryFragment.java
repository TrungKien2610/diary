package com.example.kien.diary_application;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.CardView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryDetailDiaryFragment extends Fragment {
    protected RecyclerView rcv_diary;
    protected ArrayList<Note> notes = new ArrayList<Note>();


    public CategoryDetailDiaryFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        MyDatabaseHelper db = new MyDatabaseHelper(getActivity());
//        db.createDefaultNotesIfNeed();
//        ArrayList<Note> list = db.getAllNote();
        Note note = new Note("AAA","BBB");
//        notes.addAll(list);
        notes.add(note);

        CategoryDetailDiaryAdapter adapter = new CategoryDetailDiaryAdapter(notes);
        rcv_diary.setAdapter(adapter);
//        registerForContextMenu(this.listView);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_category_detail_diary, null, false);
        Context context = getActivity();
        LinearLayoutManager manager = new LinearLayoutManager(context);
        rcv_diary.setLayoutManager(manager);
        return rootview;
    }

}
