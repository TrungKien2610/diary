package com.example.kien.diary_application;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ViewActivity extends AppCompatActivity {
    private static final int MODE_CREATE = 1;
    private static final int MODE_EDIT = 2;

    private int mode;
    Bitmap bp;

    String savePath;
    String picturePath;
    Uri selectedUriImage;
    Bitmap selectedBitmap;


    private boolean needRefresh;
    private EditText textTitle;
    private EditText textContent;
    private Button btn_save;
    private Button btn_back;
    private Spinner s;
    private String type;
    Note note;
    private ImageView img_photoCapture;
    private ImageView img_photoCapture2;

    private static final int REQUEST_ID_IMAGE_CAPTURE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        btn_back = (Button) findViewById(R.id.button_back);
        btn_save = (Button) findViewById(R.id.button_save);
        textTitle = (EditText) findViewById(R.id.text_note_title);
        textContent = (EditText) findViewById(R.id.text_note_content);
        img_photoCapture = (ImageView) findViewById(R.id.img_photoCapture);
        img_photoCapture2 = (ImageView)findViewById(R.id.img_photoCapture2);
        s = (Spinner) findViewById(R.id.spin_noteType);
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ViewActivity.this, parent.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
                type = s.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        Intent intent = this.getIntent();
//        this.note = (Note) intent.getSerializableExtra("note");
//        this.textTitle.setText(note.getNoteTitle());
//        this.textContent.setText(note.getNoteContent());

        Intent intent = this.getIntent();
        this.note = (Note) intent.getSerializableExtra("note");
        if (note == null) {
            this.mode = MODE_CREATE;
        } else {
            this.mode = MODE_EDIT;
            if (type == "Home") {
                s.setSelection(0);
            } else if (type == "Work") {
                s.setSelection(1);
            } else if (type == "Daily") {
                s.setSelection(2);
            } else if (type == "Secret") {
                s.setSelection(3);
            }
            this.textTitle.setText(note.getNoteTitle());
            this.textContent.setText(note.getNoteContent());
        }
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave(v);
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBack(v);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.menu_edit) {
//            return true;
//        }else if(id == R.id.menu_photo){
//
//        }
        switch (id) {
            case R.id.menu_edit: {
                Toast.makeText(ViewActivity.this, "Edit clicked", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.menu_photo: {
                Toast.makeText(ViewActivity.this, "Photo clicked", Toast.LENGTH_LONG).show();
                captureImage();

                break;
            }
            case R.id.menu_hengio: {
                Toast.makeText(ViewActivity.this, "Hen gio clicked", Toast.LENGTH_LONG).show();

                break;
            }
            case R.id.menu_xoa: {
                Toast.makeText(ViewActivity.this, "Delete clicked", Toast.LENGTH_LONG).show();

                break;
            }

        }


        return super.onOptionsItemSelected(item);
    }

    public void onSave(View view) {
        MyDatabaseHelper db = new MyDatabaseHelper(this);


        String title = this.textTitle.getText().toString();
        String content = this.textContent.getText().toString();

        if (title.equals("") || content.equals("")) {
            Toast.makeText(getApplicationContext(),
                    "Please enter title & content", Toast.LENGTH_LONG).show();
            return;
        }

        if (mode == MODE_CREATE) {
            this.note = new Note(type, title, content);
            db.addNote(note);
        } else {
            Log.e("VIewActivity", "onSave" + type + title + content);
            this.note.setNoteType(type);
            this.note.setNoteTitle(title);
            this.note.setNoteContent(content);
            db.updateNote(note);

        }

        this.needRefresh = true;
        this.onBackPressed();
    }

    public void onBack(View view) {
        this.onBackPressed();
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        File dir = Environment.getExternalStorageDirectory();
//        if(!dir.exists()){
//            dir.mkdirs();
//        }
//        savePath = dir.getAbsolutePath() + "/image1.png";
//        File imageFile = new File(savePath);
//        Uri uriImage = Uri.fromFile(imageFile);
//        Toast.makeText(ViewActivity.this,savePath,Toast.LENGTH_LONG).show();
//        intent.putExtra(MediaStore.EXTRA_OUTPUT,uriImage);

        this.startActivityForResult(intent, REQUEST_ID_IMAGE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ID_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {

                selectedUriImage = data.getData();
                bp = (Bitmap)data.getExtras().get("data");
                picturePath=getPicturePath(selectedUriImage);
                File image = new File(picturePath);
                img_photoCapture.setImageBitmap(bp);
                onRequestBitmapFromPathImage();



//                //lấy thumbnail để tối ưu bộ nhớ
//                selectedBitmap=getThumbnail(picturePath);
////                selectedBitmap=rotateImageIfRequired(selectedBitmap,selectedUriImage);
//                img_photoCapture.setImageBitmap(selectedBitmap);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Action canceled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Action Failed", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(ViewActivity.this, "Error capture image", Toast.LENGTH_LONG).show();
        }
    }
    public Bitmap getThumbnail(String pathHinh)
    {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathHinh, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1))
            return null;
        int originalSize = (bounds.outHeight > bounds.outWidth) ?
                bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / 500;
        return BitmapFactory.decodeFile(pathHinh, opts);
    }

    public String getPicturePath(Uri uriImage)
    {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uriImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String path = cursor.getString(columnIndex);
        cursor.close();
        return path;
    }
    private int getRotation() {
        String[] filePathColumn = {MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = getContentResolver().query(selectedUriImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int rotation =0;
        rotation = cursor.getInt(0);
        cursor.close();
        return rotation;
    }
    protected void onRequestBitmapFromPathImage(){
        MyAsyncTask requestBitmap = new MyAsyncTask();
        requestBitmap.execute(picturePath);

    }
    public class MyAsyncTask extends AsyncTask<String,Void,Bitmap>{

        @Override
        protected Bitmap doInBackground(String... params) {
            Log.e("DoInBAckground",params[0]);
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//            Bitmap bitmap = BitmapFactory.decodeFile(params[0], options);
//            try {
//                URL url = new URL(params[0]);
//                if(url == null){
//                    Log.d("TAGURL", "k co url");
//
//                }else {
//                    Log.d("TAGURL", "co url");
//                }
//                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                InputStream input = connection.getInputStream();
//                Bitmap myBitmap = BitmapFactory.decodeStream(input);
//                return myBitmap;
//            } catch (java.io.IOException e) {
//                e.printStackTrace();
//                return  null;
//            }

            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(selectedUriImage);
                final Bitmap selectedImageBitmap = BitmapFactory.decodeStream(imageStream);
                return  selectedImageBitmap;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }



        }
        //view là cai de hien thi du lieu, hien thi kieu tabblet, cung 1 du lieu nhg co nh cach hien thi khac nhau, control xac dinh chg trinh minh chay nhu nao, dieu huong chuong trinh, model la doi tuong mo ta du lieu, la thanh phan chua cac phg thuc xu li, truy xuat dâtbase

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(bitmap != null){
                Log.d("TAG", " co bitmap");
            }else{
                Log.d("TAG","ko co bitmap");
            }
            img_photoCapture2.setImageBitmap(bp);
//            img_photoCapture2.setImageResource(R.drawable.background2);

        }
    }
    private  Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) {

        // Detect rotation
        int rotation=getRotation();
        if(rotation!=0){
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
            img.recycle();
            return rotatedImg;
        }else{
            return img;
        }
    }


    @Override
    public void finish() {
        Log.e("DIARY_APP", "connected finish()");
        Intent data = new Intent();
        data.putExtra("needRefresh", needRefresh);
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }

}
