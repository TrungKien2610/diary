package com.example.kien.diary_application;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by Kien on 7/22/2016.
 */
public class CategoryDetailDiaryAdapter extends RecyclerView.Adapter<CategoryDetailDiaryAdapter.CateViewHolder> {
    protected ArrayList<Note> notes;
    public CategoryDetailDiaryAdapter(ArrayList<Note> listnote){
        this.notes = listnote;
    }

    public static class CateViewHolder extends RecyclerView.ViewHolder{
        protected TextView tv_title;
//        protected ImageView img_mode;

        public CateViewHolder(View itemview){
            super(itemview);
            tv_title = (TextView)itemview.findViewById(R.id.tv_title);
//            img_mode = (ImageView)itemview.findViewById(R.id.imageView);
        }
    }
    @Override
    public CateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemview = inflater.inflate(R.layout.item_category_detail_diary_note,null,false);
        CateViewHolder viewHolder = new CateViewHolder(itemview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CateViewHolder holder, int position) {
        Note note = notes.get(position);
        holder.tv_title.setText(note.getNoteTitle());
        // chua lam anh

    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

}
