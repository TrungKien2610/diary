package com.example.kien.diary_application;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.renderscript.Sampler;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Kien on 7/22/2016.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE = "Diary Database";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_DIARY = " Diary";
    private static final String COLUMN_DIARY_ID = "id";
    private static final String COLUMN_DIARY_TYPE = "type";
    private static final String COLUMN_DIARY_TITLE = "title";
    private static final String COLUMN_DIARY_CONTENT = "content";




    public MyDatabaseHelper(Context context) {
        super(context, DATABASE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String diary_create = "CREATE TABLE " + TABLE_DIARY + "( " + COLUMN_DIARY_ID + " INTEGER PRIMARY KEY, " + COLUMN_DIARY_TYPE + " TEXT, "+ COLUMN_DIARY_TITLE + " TEXT, " + COLUMN_DIARY_CONTENT + " TEXT )";
        db.execSQL(diary_create);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXSIST " + TABLE_DIARY);
        onCreate(db);

    }
    public void createDefaultNotesIfNeed()  {
        int count = this.getNoteCount();
        if(count ==0 ) {
            Note note1 = new Note("Home","Note Example 1",
                    "to do 1");
            Note note2 = new Note("Work","Note Example 2",
                    "to do 2");
            this.addNote(note1);
            this.addNote(note2);
        }
    }


    public void addNote(Note note) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_DIARY_TYPE,note.getNoteType());
        contentValues.put(COLUMN_DIARY_TITLE, note.getNoteTitle());
        contentValues.put(COLUMN_DIARY_CONTENT, note.getNoteContent());

        long check = db.insert(TABLE_DIARY, null, contentValues);
        if (check == -1) {
            Log.i("TAG", "insert error");
        }
        db.close();
    }

    public Note getNote(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_DIARY, new String[]{COLUMN_DIARY_ID, COLUMN_DIARY_TYPE, COLUMN_DIARY_TITLE, COLUMN_DIARY_CONTENT}, COLUMN_DIARY_ID + " =?", new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        Note result = new Note(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3));

        return result;
    }
    public ArrayList<Note> getAllNote(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Note> notes = new ArrayList<Note>();
        String all_query = "SELECT * FROM " + TABLE_DIARY;
        Cursor cursor = db.rawQuery(all_query,null);
        if (cursor.moveToFirst()){
            do{
                Note obj = new Note(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3));
                notes.add(obj);

            }while(cursor.moveToNext());
        }
        return notes;
    }
    public int getNoteCount(){
        String count_query = "SELECT * FROM " + TABLE_DIARY;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(count_query,null);
        int count = cursor.getCount();
        db.close();

        return count;
    }
    public void updateNote(Note note){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_DIARY_TYPE,note.getNoteType());
        contentValues.put(COLUMN_DIARY_TITLE,note.getNoteTitle());
        contentValues.put(COLUMN_DIARY_CONTENT,note.getNoteContent());
        int check = db.update(TABLE_DIARY,contentValues,COLUMN_DIARY_ID + " =?",new String[]{String.valueOf(note.getNoteID())});
        Log.i("MyDatabaseHelper"," update in row " + check);
        db.close();
    }
    public void deleteNote(Note note){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_DIARY,COLUMN_DIARY_ID + " = ?",new String[]{String.valueOf(note.getNoteID())});
        db.close();
    }

}
