package com.example.kien.diary_application;

import java.io.Serializable;

/**
 * Created by Kien on 7/22/2016.
 */
public class Note implements Serializable{
    private int noteID;
    private String noteTitle;
    private String noteContent;
    private String noteType;


    public Note() {
    }

    public Note(String noteTitle, String noteContent) {
        this.noteTitle = noteTitle;
        this.noteContent = noteContent;
    }
    public Note(String noteType, String noteTitle,String noteContent) {
        this.noteTitle = noteTitle;
        this.noteContent = noteContent;
        this.noteType = noteType;
    }




    public Note(int noteID,String noteType, String noteTitle, String noteContent) {
        this.noteID = noteID;
        this.noteType = noteType;
        this.noteTitle = noteTitle;
        this.noteContent = noteContent;
    }

    public int getNoteID() {
        return noteID;
    }

    public void setNoteID(int noteID) {
        this.noteID = noteID;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }
}
