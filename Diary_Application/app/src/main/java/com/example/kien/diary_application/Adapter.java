package com.example.kien.diary_application;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Kien on 7/23/2016.
 */
public class Adapter extends BaseAdapter {
    protected ArrayList<Note> list;
    protected Context context;

    public Adapter(ArrayList<Note> ds, Context context) {
        this.list = ds;
        this.context = context;
    }

    public static class ViewHolder {
        protected TextView tv_type;
        protected TextView tv_title;
        protected ImageView img_icon;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_category_detail_diary_note, null, false);
            holder = new ViewHolder();
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tv_type = (TextView) convertView.findViewById(R.id.tv_type);
            holder.img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Note obj = (Note) getItem(position);
        holder.tv_title.setText(obj.getNoteTitle());
        holder.tv_type.setText(obj.getNoteType());

        if (obj.getNoteType() == "Home") {
            holder.img_icon.setImageResource(R.drawable.background1);
        } else if (obj.getNoteType() == "Work") {
            holder.img_icon.setImageResource(R.drawable.background2);
        } else if (obj.getNoteType() == "Daily") {
            holder.img_icon.setImageResource(R.drawable.background1);
        } else if (obj.getNoteType() == "Secret") {
            holder.img_icon.setImageResource(R.drawable.background2);
        }


        return convertView;
    }
}
